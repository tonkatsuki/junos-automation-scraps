from jnpr.junos import Device
from lxml import etree

dev = Device(host='10.0.10.3', user='admin', password='Cisco123', gather_facts=False)
dev.open()

with dev:
    makesnapshot = dev.rpc.request_snapshot()
    getsnapshot = dev.rpc.get_snapshot_information()
    print(etree.tostring(getsnapshot, encoding='unicode'))
