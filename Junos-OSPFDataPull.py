from netmiko import ConnectHandler
import json

junos = {
    "device_type": "juniper_junos",
    "host": "10.0.13.56",
    "username": "admin",
    "password": "Cisco123"
}
device = ConnectHandler(**junos)

data = json.loads(device.send_command('show ospf neighbor | display json no-export-path'))
# use this below to get the IP
# data2 = json.loads(device.send_command('show configuration interfaces xe-0/2/0 | display json no-export-path'))

print(json.dumps(data, indent=4))

Filtered_List = data['ospf-neighbor-information']
for i in range (0,len(Filtered_List)):
    NewDictionary = Filtered_List[i]['Keys']
    if NewDictionary[]