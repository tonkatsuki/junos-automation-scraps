#!/usr/bin/env python3

import paramiko

host = '10.0.13.192'
hostUsername = 'cisco'
hostPassword = 'cisco'

targetList = [
    '10.0.13.118',
    '10.0.13.121'
]

fileList = [
    'examplefile.txt',
    'examplefile2.txt'
]

for i in list:
    client = paramiko.client.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, username=hostUsername, password=hostPassword)
    for f in fileList:
        _stdin, _stdout,_stderr = client.exec_command("scp ~/ex3400/" + f + " juniper-admin@" + i + ":/var/tmp/" + f)
        print(_stdout.read().decode())
    
    client.close()