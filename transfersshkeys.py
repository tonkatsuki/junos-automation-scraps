#!/usr/bin/env python3
import paramiko
from jnpr.junos import Device
from jnpr.junos.utils.config import Config

host = "10.0.0.1"
hostUsername = "cisco"
hostPassword = "cisco"
list = [
    "10.0.20.1"
]

for i in list:
    client = paramiko.client.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, username=hostUsername, password=hostPassword)
    _stdin, _stdout,_stderr = client.exec_command('cat ~/.ssh/id_ed25519.pub')
    publicKey = (_stdout.read().decode())
    command = ('set system login user cisco authentication ssh-ed25519 \"{}\"'.format(publicKey.strip()))
    print(command)
    device = Device(host=i,
                    user='cisco',
                    password='asd',
                    gather_facts=False)
    device.open()
    configuration = Config(device)
    configuration.load(command, format='set')
    configuration.commit()
    device.close()